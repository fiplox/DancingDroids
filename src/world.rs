use rand::Rng;

/// The World is represented here.
pub struct World {
    pub x: i32,
    pub y: i32,
}

/// Create a random World.
pub fn random_world() -> World {
    let mut rng = rand::thread_rng();
    let z = rng.gen_range(5, 45);
    World { x: z, y: z }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::any::type_name;

    fn type_of<T>(_: T) -> &'static str {
        type_name::<T>()
    }

    #[test]
    fn test_random_world() {
        let w = random_world();
        assert_eq!(type_of(w), "dancing_droid::world::World");
    }
}
