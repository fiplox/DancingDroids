# DancingDroids v0.3.1

#### _When droids dance togethers_

## Description

DancingDroids is a huge pile of stinky shit that looks like a game played by your computer. \
The aim is to display a grid with robots moving in it, until:
- they finish all instructions 
- they collide 
- a robot goes off the map



The size of the grid, robot's positions and their move are preconfigured in a file that has to be correctly formated <- Don't mess with that. \
If you leave some instruction lines blank, they will be generated for you. \
You can find more information in the [subject](https://framagit.org/mhart/DancingDroids/-/blob/master/SUBJECT.md).

## Flags

```
 -h, --help            Prints help information
 -V, --version         Prints version information
 -w, --random-world    Generate random world
 -r, --random 	       Generate random world with random robots
 -f, --file <file>     Configuration file
```
## Authors

- Martin HART
- Volodymyr PATUTA
- Stephane Elias BENABDESLAM

## Disclaimer

In order to read the code of this project you need to use sunglasses.
Please, make sure you keep breathing :heart:
